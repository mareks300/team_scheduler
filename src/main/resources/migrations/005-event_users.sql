CREATE TABLE event_users
(
   id             INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
   user_id      INT UNSIGNED  NOT NULL,
   event_id      INT UNSIGNED  NOT NULL,
   date_sent   DATE NOT NULL,
   date_accepted DATE,
   date_declined DATE,
   
   FOREIGN KEY (user_id)
      REFERENCES users (id)
      ON DELETE CASCADE,

   FOREIGN KEY (event_id)
      REFERENCES events (id)
      ON DELETE CASCADE
);