CREATE TABLE event_teams
(
   id             INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
   team_id      INT UNSIGNED  NOT NULL,
   event_id      INT UNSIGNED  NOT NULL,

   FOREIGN KEY (team_id)
      REFERENCES teams (id)
      ON DELETE CASCADE,

   FOREIGN KEY (event_id)
      REFERENCES events (id)
      ON DELETE CASCADE
);