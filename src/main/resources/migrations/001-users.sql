CREATE TABLE users
(
	id         INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	name       VARCHAR(32) NOT NULL,
	surname    VARCHAR(32) NOT NULL,
	email  VARCHAR(32)        NOT NULL

);

CREATE UNIQUE INDEX ux_users_email
    ON users(email);