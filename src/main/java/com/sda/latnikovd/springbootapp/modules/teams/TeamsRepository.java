package com.sda.latnikovd.springbootapp.modules.teams;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
interface TeamsRepository extends JpaRepository<Teams, Long> {
}
