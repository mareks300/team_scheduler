package com.sda.latnikovd.springbootapp.modules.eventTeams;
import javax.persistence.*;

@Entity
@Table(name = "event_teams")
public class EventTeams {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column
    private Long id;

    @Column
    private Long teamId;

    @Column
    private Long eventId;

    public EventTeams(Long teamId, Long eventId) {
        this.teamId = teamId;
        this.eventId = eventId;
    }

    public Long getId(Long id) {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTeamId(Long id) {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    @Override
    public String toString() {
        return "eventTeams{" +
                "id=" + id +
                ", teamId=" + teamId +
                ", eventId=" + eventId +
                '}';
    }
}
