package com.sda.latnikovd.springbootapp.modules.users;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository <User, Long> {
}
