package com.sda.latnikovd.springbootapp.modules.eventusers;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class EventUsersService {

    @Autowired EventUsersRepository eventUsersRepository;

    @Transactional(readOnly = true)
    public List<EventUsers> findAll(){
        return eventUsersRepository.findAll();
    }

    // metode, kas pec lietotaja un eventa atgriez event user objektu
    @Transactional(rollbackFor = Exception.class)
    public void approve(long eventId, long userId){

        EventUsers eventUser = eventUsersRepository.findByUserIdAndEventId(userId, eventId).orElse(null);
        Validate.notNull(eventUser, "eventUser is undefined for userId '%s' and eventId '%s'", userId, eventId);
        eventUser.setDateAccepted(new Date());
        eventUser.setDateDeclined(null);
        eventUsersRepository.save(eventUser);
    }
}
