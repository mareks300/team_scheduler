package com.sda.latnikovd.springbootapp.modules.teams;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TeamsService {

    @Autowired
    private TeamsRepository teamsRepository;

    @Transactional(readOnly = true)
    public List<Teams> findAll() {
        return teamsRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public Teams save(String name) {
        Teams teams = new Teams(name);
        return teamsRepository.save(teams);
    }

    @Transactional(rollbackFor = Exception.class)
    public void delete(final long id) {
        teamsRepository.deleteById(id);
    }


}
