package com.sda.latnikovd.springbootapp.modules.eventTeams;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventTeamsRepository extends JpaRepository<EventTeams, Long> {
    List<EventTeams> findByTeamId(Long id);
}
