package com.sda.latnikovd.springbootapp.modules.userteams;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserTeamsService {

    @Autowired
    private UserTeamsRepository userTeamsRepository;


    @Transactional(readOnly = true)
    public List<UserTeams> findAll() {

        return userTeamsRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<Long> findByUserId(Long id) {
        List<UserTeams> userTeams = userTeamsRepository.findByUserId(id);
        List<Long> teamsId = new ArrayList<>();

        for (UserTeams userTeam : userTeams) {
            teamsId.add(userTeam.getTeamId());
        }

        return teamsId;
    }

    @Transactional(readOnly = true)
    public List<Long> findByUserTeamsId(Long id) {
        List<UserTeams> userTeams = userTeamsRepository.findByTeamId(id);
        List<Long> userTeamsId = new ArrayList<>();

        for (UserTeams userTeam : userTeams) {
            userTeamsId.add(userTeam.getId());
        }
        return userTeamsId;


    }


}
