package com.sda.latnikovd.springbootapp.modules.eventTeams;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class EventTeamsService {

    @Autowired
    private EventTeamsRepository eventTeamsRepository;

    @Transactional(readOnly = true)
    public List<EventTeams> findAll() {
        return eventTeamsRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<Long> findByTeamId(Long id) {
        List<EventTeams> eventTeams = eventTeamsRepository.findByTeamId(id);
        List<Long> eventsId = new ArrayList<>();

        for (EventTeams eventTeam : eventTeams) {
            eventsId.add(eventTeam.getEventId());
        }
        return eventsId;
    }

}
