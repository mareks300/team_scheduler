package com.sda.latnikovd.springbootapp.modules.userteams;

import com.sda.latnikovd.springbootapp.modules.teams.Teams;
import com.sda.latnikovd.springbootapp.modules.users.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public
interface UserTeamsRepository extends JpaRepository<UserTeams, Long> {
    List<UserTeams> findByUserId(Long Id);

    List<UserTeams> findByTeamId(Long id);

}
