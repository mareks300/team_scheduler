package com.sda.latnikovd.springbootapp.modules.eventusers;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "event_users")
public class EventUsers {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;
    @Column
    private Long eventId;
    @Column
    private Long userId;

    @Column
    private Date dateSent;
    @Column
    private Date dateAccepted;
    @Column
    private Date dateDeclined;

    public EventUsers(Long id, Long eventId, Long userId, Date dateSent, Date dateAccepted, Date dateDeclined) {
        this.id = id;
        this.eventId = eventId;
        this.userId = userId;
        this.dateSent = dateSent;
        this.dateAccepted = dateAccepted;
        this.dateDeclined = dateDeclined;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Date getDateSent() {
        return dateSent;
    }

    public void setDateSent(Date dateSent) {
        this.dateSent = dateSent;
    }

    public Date getDateAccepted() {
        return dateAccepted;
    }

    public void setDateAccepted(Date dateAccepted) {
        this.dateAccepted = dateAccepted;
    }

    public Date getDateDeclined() {
        return dateDeclined;
    }

    public void setDateDeclined(Date dateDeclined) {
        this.dateDeclined = dateDeclined;
    }

    @Override
    public String toString() {
        return "EventUsers{" +
                "id=" + id +
                ", eventId=" + eventId +
                ", userId=" + userId +
                ", dateSent=" + dateSent +
                ", dateAccepted=" + dateAccepted +
                ", dateDeclined=" + dateDeclined +
                '}';
    }
}

