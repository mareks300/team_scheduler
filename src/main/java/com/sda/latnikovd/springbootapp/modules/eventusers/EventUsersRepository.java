package com.sda.latnikovd.springbootapp.modules.eventusers;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
interface EventUsersRepository extends JpaRepository<EventUsers,Long> {

    Optional<EventUsers> findByUserIdAndEventId(long userId, long eventId);
}
