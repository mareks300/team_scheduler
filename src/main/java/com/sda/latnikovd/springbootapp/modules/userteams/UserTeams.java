package com.sda.latnikovd.springbootapp.modules.userteams;


import javax.persistence.*;

@Entity
@Table(name = "user_teams")
public class UserTeams {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column
    private Long id;

    @Column
    private Long userId;

    @Column
    private Long teamId;

    public UserTeams(final Long userId, final Long teamId) {
        this.teamId = teamId;
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    @Override
    public String toString() {
        return "userTeams{" +
                "id=" + id +
                ", userId=" + userId +
                ", teamId=" + teamId +
                '}';
    }
}

