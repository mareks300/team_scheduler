package com.sda.latnikovd.springbootapp.modules.events;

import com.sda.latnikovd.springbootapp.modules.eventTeams.EventTeams;
import com.sda.latnikovd.springbootapp.modules.eventTeams.EventTeamsRepository;
import com.sda.latnikovd.springbootapp.modules.eventTeams.EventTeamsService;
import com.sda.latnikovd.springbootapp.modules.eventusers.EventUsersService;
import com.sda.latnikovd.springbootapp.modules.teams.TeamsService;
import com.sda.latnikovd.springbootapp.modules.users.User;
import com.sda.latnikovd.springbootapp.modules.users.UsersService;
import com.sda.latnikovd.springbootapp.modules.userteams.UserTeamsRepository;
import com.sda.latnikovd.springbootapp.modules.userteams.UserTeamsService;
import org.apache.commons.lang3.Validate;
import org.hibernate.annotations.LazyToOne;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import javax.xml.bind.ValidationEvent;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class EventService {

    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private UserTeamsService userTeamsService;
    @Autowired
    private UsersService usersService;
    @Autowired
    private TeamsService teamsService;
    @Autowired
    private EventTeamsRepository eventTeamsRepository;
    @Autowired
    private EventTeamsService eventTeamsService;
    @Autowired
    private EventUsersService eventUsersService;
    @Autowired
    private UserTeamsRepository userTeamsRepository;


    @Transactional(readOnly = true)
    public List<Event> findAll() {
        return eventRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public Event save(final Event event) {
        return eventRepository.save(event);
    }

    @Transactional(rollbackFor = Exception.class)
    public void delete(final long id) {
        eventRepository.deleteById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Event update(final Event event) {
        Event originalEvent = eventRepository.findById(event.getId()).orElse(null);
        Validate.notNull(originalEvent, "Cant find event with ID %s", event.getId());

        Date eventDate = originalEvent.getDate();
        Date currentDate = new Date();
        long diffInMillies = Math.abs(currentDate.getTime() - eventDate.getTime());
        long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
        Validate.isTrue(diff <= 1, "Event cannot be updated within 24 hours before the event date");
    }



    @Transactional(rollbackFor = Exception.class)
    public void approve(Long userId, Long eventId, Long teamId) {
        //Need to find user
        User user = usersService.findById(userId);
        //get teams of user
        List<Long> userTeamIds = userTeamsService.findByUserTeamsId(teamId);
        //get event
        Long event = eventId;
        //get event teams
        List<Long> eventTeamsIds = eventTeamsService.findByTeamId(eventId);
        //need to check if user is in one of teams
        ///for cikla iet cauri vienam listam un parbaudit vai otra lista ir elementi
        boolean userBelongToTheTeams = false;
        for (Long userTeamId : userTeamIds) {

            if (eventTeamsIds.contains(userTeamId)) {
                eventUsersService.approve(eventId, userId);
                return;
            }
        }

        //need to check if team is in event


    }


}


