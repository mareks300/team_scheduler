package com.sda.latnikovd.springbootapp.modules.users;

import com.sun.xml.internal.ws.api.message.ExceptionHasMessage;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.bind.ValidationEvent;
import java.util.List;

@Service
public class UsersService {

    @Autowired
    private UserRepository userRepository;

    @Transactional(readOnly = true)
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Transactional(readOnly = true)
    public User findById(Long id) {
        return userRepository.findById(id).get();
    }

    @Transactional(rollbackFor = Exception.class)
    public User update(final User user) {
        User originalUser = userRepository.findById(user.getId()).orElse(null);
        Validate.notNull(originalUser, "Cant find user with ID %s",user.getId());
        Validate.isTrue(user.getEmail().equals(originalUser.getEmail()), "Cant update %s to %s!", user.getEmail(), originalUser.getEmail());
        return userRepository.save(user);
    }

    
    @Transactional(rollbackFor = Exception.class)
    public User save(final User user) { //

        Validate.notNull(user, "user is undefined");
        Validate.notBlank(user.getName(), "name is blank for user '%s' ", user);
        Validate.notBlank(user.getSurname(), "surname is blank for user'%s'", user);
        Validate.notBlank(user.getEmail(), "email is blank for user '%s'", user);


        return userRepository.save(user);
    }


    @Transactional(rollbackFor = Exception.class)
    public void delete(final long id) {
        userRepository.deleteById(id);
    }

}

// E-mail kolona, mes vinu panemam un pārbaudam, ja dati sakrīt, tad mēs turpinam update, ja nesakrīt tad mēs izdodam kļudu.