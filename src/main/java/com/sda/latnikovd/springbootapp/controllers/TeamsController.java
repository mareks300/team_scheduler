package com.sda.latnikovd.springbootapp.controllers;

import com.sda.latnikovd.springbootapp.modules.teams.Teams;
import com.sda.latnikovd.springbootapp.modules.teams.TeamsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.naming.Name;
import java.util.List;

@RestController
@RequestMapping("/teams")
public class TeamsController {

    @Autowired
    private TeamsService teamsService;

    @GetMapping
    public List<Teams> findAll() { return teamsService.findAll(); }

    @PutMapping
    public Teams create(@RequestBody String name) {
        return teamsService.save(name);
    }

    // this method will be run when a DELETE request is sent to url "http://localhost:8080/authors/9" where 9 is ID of author to delete
    @DeleteMapping("/{Id}")
    public void delete(@PathVariable long Id) {
        teamsService.delete(Id);
    }




}
