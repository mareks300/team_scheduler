package com.sda.latnikovd.springbootapp.controllers;

import com.sda.latnikovd.springbootapp.modules.users.User;
import com.sda.latnikovd.springbootapp.modules.users.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController

@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UsersService usersService;

    @GetMapping
    public List<User> findAll() {
        return usersService.findAll();
    }

//    @GetMapping
//    private List <User> findById () {
//        return usersService.findById ();
//    }

    @PutMapping
    public User create(@RequestBody User user) {
        return usersService.save(user);
    }

    @PatchMapping
    public User update(@RequestBody User user) {
        return usersService.update(user);
    }

    @DeleteMapping("/{Id}")
    public void delete(@PathVariable long id) {
        usersService.delete(id);
    }


}
