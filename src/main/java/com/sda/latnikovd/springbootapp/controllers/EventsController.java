package com.sda.latnikovd.springbootapp.controllers;

import com.sda.latnikovd.springbootapp.modules.events.Event;
import com.sda.latnikovd.springbootapp.modules.events.EventService;
import com.sda.latnikovd.springbootapp.modules.teams.Teams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Id;
import java.awt.*;
import java.util.List;

@RestController
@RequestMapping("/events")
public class EventsController {
    @Autowired
    private EventService eventService;

    @GetMapping
    private List<Event> findAll() {
        return eventService.findAll();
    }

    @PatchMapping
    public Event update(@RequestBody Event event) {
        return eventService.update(event);
    }

    @PutMapping
    public Event create(@RequestBody Event event) {
        return eventService.save(event);
    }
    @DeleteMapping("/{Id}")
    public void delete(@PathVariable long Id) {
        eventService.delete(Id);
    }


}

